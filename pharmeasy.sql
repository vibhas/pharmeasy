-- phpMyAdmin SQL Dump
-- version 4.7.5
-- https://www.phpmyadmin.net/
--
-- Host: Localhost
-- Generation Time: Nov 24, 2017 at 02:56 AM
-- Server version: 5.7.20-0ubuntu0.16.04.1
-- PHP Version: 7.1.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `pharmeasy`
--

-- --------------------------------------------------------

--
-- Table structure for table `reports`
--

CREATE TABLE `reports` (
  `id` int(11) NOT NULL,
  `userid` int(11) NOT NULL,
  `prescriptions` text,
  `softcopy` text,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `reports`
--

INSERT INTO `reports` (`id`, `userid`, `prescriptions`, `softcopy`, `created_at`, `updated_at`) VALUES
(3, 5, 'Crocin', NULL, '2017-11-23 00:00:00', '2017-11-23 00:00:00'),
(4, 5, 'Diabetes', NULL, '2017-11-23 00:00:00', '2017-11-23 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `requests`
--

CREATE TABLE `requests` (
  `id` int(11) NOT NULL,
  `request_userid` int(11) NOT NULL,
  `target_userid` int(11) NOT NULL,
  `is_allowed` enum('-1','1','0','2') NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `requests`
--

INSERT INTO `requests` (`id`, `request_userid`, `target_userid`, `is_allowed`, `created_at`, `updated_at`) VALUES
(2, 7, 5, '-1', '2017-11-23 21:38:06', '2017-11-23 22:18:35');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `mobile` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `mobile`, `name`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(5, '9999999999', 'Tom', '$2y$10$MB77qoXf9Ig3P1Uw9h.eueM8ONDn9WxBEydCXpr1SW5opo.FZZ3Ci', 'X4qn6juiHSdjVz9XNoxL1aWzdVpD2QwAn9sWHqdPJwmj8KdMCOCVBMz7kTyD', '2017-11-23 21:37:00', '2017-11-23 21:37:00'),
(6, '8888888888', 'Sai Medical Stores', '$2y$10$8udYX.x5LrSGY5VjiNV1m.leQu4x.GCsNr/yxcfc8avmx83AXn.xy', 'AUtHS4VU3abyPPnd9bymWGDunmbrVpct03gFcsqnCImuQAbVHVnDewXhaBn7', '2017-11-23 21:37:36', '2017-11-23 21:37:36'),
(7, '7777777777', 'Dr . Ashwin Gupta', '$2y$10$VEcHDyUzo4H.P/TD/oke.eoDbgjTEpDu4V5QMgcEpbIdDxiIaSZTO', 'ZETgvaGiTsmtPuTp9KK427KbA0XyicBvm1X8wQKI7eC98YwqEyFNHmzPN3nw', '2017-11-23 21:37:57', '2017-11-23 21:37:57');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `reports`
--
ALTER TABLE `reports`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idx_userid` (`userid`);

--
-- Indexes for table `requests`
--
ALTER TABLE `requests`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idx_requestuserid` (`request_userid`),
  ADD KEY `idx_targetuserid` (`target_userid`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `idx_moble` (`mobile`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `reports`
--
ALTER TABLE `reports`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `requests`
--
ALTER TABLE `requests`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
