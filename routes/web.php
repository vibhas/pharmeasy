<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/myrequests', 'HomeController@myrequests')->name('myrequests');

Route::get('/updaterequest/{id}/{flag}', 'HomeController@updaterequest')->name('updaterequest');

Route::get('/listrecords/{id}', 'HomeController@listrecords')->name('listrecords');

Route::get('/profile', 'HomeController@profile')->name('profile');

Route::post('/sendrequests','HomeController@storeRequests')->name('sendrequests');
