<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use App\User;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $totalRequests = DB::table('requests')->select('requests.id')->join('users','requests.request_userid','=','users.id')->whereNotIn('is_allowed',['2','0'])->whereRaw("( target_userid=".Auth::id()." or  request_userid=".Auth::id()." ) ")->get()->toArray();


        return view('home')->with('totalRequests',count($totalRequests));
    }

    public function storeRequests(Request $request)
    {
       try 
       {
        $validator = Validator::make($request->all(), ['mobile' => 'required']);

         if ($validator->fails()):

                return redirect('/home')->withErrors($validator)->with('status-failed', 'Enter valid mobile no');

         else:
                $id=User::select('id')->where('mobile',$request->mobile)->firstOrFail()->id;
 
                if($id==Auth::id()): return redirect('/home')->with('status-failed','Plz enter valid request mobile number'); endif;
 
                $count = DB::table('requests')->where(['request_userid'=>Auth::id(),'target_userid'=>$id,'is_allowed'=>'-1'])->count();

                if(!$count):

                DB::table('requests')->insert(
                                                [
                                                    'request_userid'=>Auth::id(),
                                                    'target_userid'=>$id,
                                                    'is_allowed'=>'-1',
                                                    'created_at'=>date('Y-m-d H:i:s'),
                                                    'updated_at'=>date('Y-m-d H:i:s')
                                                ]
                );

                    return redirect('/home')->with('status-success', 'Request send successfully');

                else:

                    return redirect('/home')->with('status-failed', 'Your request is already under process');

                endif;

         endif;

        }
        catch(\Illuminate\Database\Eloquent\ModelNotFoundException $e)
        {
            return redirect('/home')->with('status-failed', 'Records doesnot exists in our DB');
        }
        catch (\Exception $e) 
        {
                return redirect('/home')->with('status-failed', 'Something went wrong');
        }

    }

    public function myrequests()
    {
        $sql=DB::raw("requests.id,users.name as requestuser,users1.name as senduser,requests.created_at,is_allowed,IF(requests.request_userid = '".Auth::id()."','1','0') as sendrequests,IF(requests.target_userid = '".Auth::id()."','1','0') as receivedrequests");

        $requests = DB::table('requests')->select($sql)->join('users','requests.request_userid','=','users.id')->join('users as users1','requests.target_userid','=','users1.id')
                    ->whereNotIn('is_allowed',['2','0'])
                    ->whereRaw("( target_userid=".Auth::id()." or  request_userid=".Auth::id()." ) ")
                    ->get()->toArray();


        return view('myrequests')->with('requests',$requests);
    }

    public function updaterequest($id,$flag)
    {
        DB::table('requests')->where(['id'=>$id,'target_userid'=>Auth::id()])->update(['is_allowed'=>$flag,'updated_at'=>date('Y-m-d H:i:s')]);

        return redirect('/myrequests')->with('status-success','Request has been updated');
    }

    public function listrecords($id)
    {
        $results=DB::table('reports')->select('reports.*')->join('requests','reports.userid','=','requests.target_userid')
                 ->where(['requests.id'=>$id,'request_userid'=>Auth::id(),'is_allowed'=>'1'])->get()->toArray();

        return view('myrecords')->with('records',$results);
    }

    public function profile()
    {
        return view('profile');
    }
}
