@extends('layouts.app')

@section('content')

<div class="container">
   
    <div class="row">
        
        <div class="col-sm-2">
            <nav class="nav-sidebar">
                <ul class="nav">
                    <li><a href="{{route('home')}}">Home</a></li>
                    <li class="active"><a href="javascript:;">Profile</a></li>
                    <li><a href="{{route('myrequests')}}">Requests</a></li>
                </ul>
            </nav>
        </div>

        <div class="col-sm-10">
                Mobile : {{ Auth::user()->mobile }}
                <br/>
                Name : {{ Auth::user()->name }}
        </div>

    </div>

</div>

@endsection