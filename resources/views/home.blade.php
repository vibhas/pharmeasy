@extends('layouts.app')

@section('content')
<div class="container">
   
    <div class="row">
        
        <div class="col-sm-2">
            <nav class="nav-sidebar">
                <ul class="nav">
                    <li class="active"><a href="javascript:;">Home</a></li>
                    <li><a href="{{route('profile')}}">Profile</a></li>
                    <li><a href="{{route('myrequests')}}">Requests @if($totalRequests) <div class="numberCircle"> {{ $totalRequests }} </div>  @endif</a></li>
                </ul>
            </nav>
        </div>

        <div class="col-sm-10">
            <div class="row">
                        @if (session('status-success'))
                        <div class="alert alert-success">
                        {{ session('status-success') }}
                        </div>
                        @endif
                        @if (session('status-failed'))
                        <div class="alert alert-danger">
                        {{ session('status-failed') }}
                        </div>
                        @endif

                        <!--  @foreach ($errors->get('mobile') as $message)
                            <strong>{{ $message }}</strong>
                        @endforeach -->
            </div>
            <div class="row">
                <form id="addrequests" action="{{ route('sendrequests') }}" method="POST" >
                    {{ csrf_field() }}
                    <div class="form-group col-lg-4">
                    <input type="text" class="form-control" id="mobile" name="mobile" placeholder="Enter Mobile to requests access ..">
                    </div>
                    <div class="form-group col-lg-2">
                        <button class="btn btn-success">Go</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
       
</div>

@endsection
