@extends('layouts.app')

@section('content')

<div class="container">
   
    <div class="row">
        
        <div class="col-sm-2">
            <nav class="nav-sidebar">
                <ul class="nav">
                    <li ><a href="{{route('home')}}">Home</a></li>
                    <li><a href="{{route('profile')}}">Profile</a></li>
                    <li class="active"><a href="javascript:;">Requests</a></li>
                </ul>
            </nav>
        </div>

        <div class="col-sm-10">
            <div class="row">
                        @if (session('status-success'))
                        <div class="alert alert-success">
                        {{ session('status-success') }}
                        </div>
                        @endif
                        @if (session('status-failed'))
                        <div class="alert alert-danger">
                        {{ session('status-failed') }}
                        </div>
                        @endif

                        <!--  @foreach ($errors->get('mobile') as $message)
                            <strong>{{ $message }}</strong>
                        @endforeach -->
            </div>
            <div class="row">
                <div class="col-lg-6">
                    <span>Incoming Request :--</span>
                @if (count($requests)>=1)
                    <ul class="listrecords">
                        @foreach($requests as $request)
                            @if($request->receivedrequests=='1')
                            <li>
                                {{ $request->requestuser }}
                                @if($request->is_allowed=='-1')
                                <button class="btn btn-success btn-sm" onclick="location.href = 'updaterequest/{{ $request->id }}/1';">Approve</button>
                                <button class="btn btn-danger btn-sm" onclick="location.href = 'updaterequest/{{ $request->id }}/0';">Reject</button>
                                @else
                                <button class="btn btn-danger btn-sm" onclick="location.href = 'updaterequest/{{ $request->id }}/2';">Revoke</button>
                                @endif
                            </li>
                            @endif
                        @endforeach
                    </ul>
                @else
                    <span>No Requests</span>
                @endif
                </div>

                <div class="col-lg-6">
                    <span>Outgoing Requests :-</span>
                @if (count($requests)>=1)
                    <ul class="listrecords">
                        @foreach($requests as $request)
                            @if($request->sendrequests=='1')
                            <li>
                                {{ $request->senduser }}
                                @if($request->is_allowed=='1')
                                    <button class="btn btn-warning btn-sm" onclick="location.href = 'listrecords/{{ $request->id }}';">View</button>
                                @endif
                            </li>
                            @endif
                        @endforeach
                    </ul>
                @else
                    <span>No Requests</span>
                @endif
                </div>
            </div>
        </div>
    </div>
       
</div>

@endsection
