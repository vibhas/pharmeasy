@extends('layouts.app')

@section('content')

<div class="container">
   
    <div class="row">
        
        <div class="col-sm-2">
            <nav class="nav-sidebar">
                <ul class="nav">
                    <li><a href="{{route('home')}}">Home</a></li>
                    <li><a href="javascript:;">Profile</a></li>
                    <li><a href="{{route('myrequests')}}">Requests</a></li>
                </ul>
            </nav>
        </div>

        <div class="col-sm-10">
        	@if (count($records) >= 1)
        		<ul>
        			@foreach ($records as $record)
					    <p>Medicine : {{ $record->prescriptions }} | Date -{{ $record->created_at }} </p>
					@endforeach
        		</ul>
        	@else
        		<span>No Records</span>
        	@endif
        </div>

    </div>

</div>
@endsection